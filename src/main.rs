///sequential differentiation
fn diff(input: Vec<i32>) -> Vec<i32> {
    let mut output: Vec<i32> = Vec::new();
    for i in 1..(input.len()) {
        output.push(input[i] - input[i-1]);
    }
    output
}

///zeroed out vector to know when to stop differentiating
fn zeros(size: usize) -> Vec<i32> {
    let mut zero_vec: Vec<i32> = Vec::with_capacity(size);
    for _i in 0..size {
        zero_vec.push(0);
    }
    return zero_vec;
}
///to write python code
fn spit_python(all: Vec<Vec<i32>>) -> String {
    let mut coefficients: Vec<i32> = Vec::new();
    for i in 0..all.len() {
        coefficients.push(all[i][0]);
    }
    let mut output = format!("for n in range(0, {}):\n\tx = ", coefficients.len());
    for i in 0..coefficients.len() {
        output.push_str(format!("math.comb(n, {}) * {}", i, coefficients[i]).as_str());
        if i != coefficients.len() - 1 {
            output.push_str(" + ");
        }
    }
    output.push_str("\n\tprint(x)");
    output
}

fn main() {
    //the sequence to write
    let start = vec![6, 22, 73, 7, 17, 21, 24, 5, 27, 53, 11, 42, 7, 21, 18, 41, 27, 333];
    //all the derived sequences
    let mut all: Vec<Vec<i32>> = Vec::new();
    all.push(start);
    //current sequence
    let mut current = diff(all[0].clone());
    //keep deriving until 0
    while current != zeros(current.len()) {
        all.push(current);
        current = diff(all[all.len() - 1].clone());
    }
    println!("{:#?}", all);
    println!("{}", spit_python(all));
}